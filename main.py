import numpy as np

from TSP import TSP

node_10 = TSP(node_size=10, print_data=False, mr=0.1, iteration=300)
node_10.start()
for i in node_10.get_top_chromosome():
    print(f'{i["road"]} ---> {i["cost"]}')

node_20 = TSP(node_size=20, print_data=False, mr=0.1, iteration=500)
node_20.start()
for i in node_20.get_top_chromosome():
    print(f'{i["road"]} ---> {i["cost"]}')

# best this matrix is 69
matrix = [[0, 7, 3, 3, 16, 5, 20, 31, 8, 43],
          [7, 0, 10, 42, 14, 41, 48, 26, 9, 20],
          [3, 10, 0, 43, 10, 8, 20, 14, 12, 6],
          [3, 42, 43, 0, 11, 7, 33, 3, 7, 2],
          [16, 14, 10, 11, 0, 33, 41, 23, 20, 25],
          [5, 41, 8, 7, 33, 0, 12, 23, 26, 11],
          [20, 48, 20, 33, 41, 12, 0, 31, 45, 19],
          [31, 26, 14, 3, 23, 23, 31, 0, 37, 47],
          [8, 9, 12, 7, 20, 26, 45, 37, 0, 22],
          [43, 20, 6, 2, 25, 11, 19, 47, 22, 0]]
for i in range(10):
    matrix = np.array(matrix)
    my_matrix = TSP(matrix=matrix, print_data=False, mr=0.1, iteration=300)
    my_matrix.start()

    for i in my_matrix.get_top_chromosome():
        print(f'{i["road"]} ---> {i["cost"]}')
