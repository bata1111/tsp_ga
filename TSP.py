import numpy as np
import random as my_rand

"""
you can pas this values
node_size[10] , count_population[6], mr[0.1], iteration[1000], print_data[True], matrix[ zeros(10*10)]
"""


class TSP:

    def __init__(self, node_size=10, count_population=6, mr=0.1, iteration=1000, print_data=True,
                 matrix=np.zeros([0, 0])):
        self.node_size = node_size
        self.print_data = print_data
        self.population = []
        self.mr = mr
        self.count_population = count_population
        self.iteration = iteration
        self.matrix = matrix
        if self.matrix.shape[0] == 0:
            self.matrix = np.zeros([node_size, node_size])
        elif not self.matrix.shape[0] == node_size or not self.matrix.shape[0] == self.matrix.shape[1]:
            raise NameError(
                f'you have to send matrix whit size {node_size}*{node_size} but send matrix whit size {self.matrix.shape[0]}*{self.matrix.shape[1]}')

        # if matrix is empty get number to it
        if self.matrix[0][1] == 0:
            for i in range(node_size):
                for j in range(node_size):
                    if i == j or self.matrix[i, j] != 0.0:
                        continue
                    else:
                        self.matrix[i][j] = np.random.randint(2, 50)
                        self.matrix[j][i] = self.matrix[i][j]

        print("************************************ START ************************************")
        print(self.matrix)

    def _mutation(self, i, population):
        a, b = np.random.choice(self.node_size, 2, replace=False)
        if a > b:
            a, b = b, a
        dist = int((b - a) / 2)
        if dist == 0:
            population[i]['road'][a], population[i]['road'][b] = population[i]['road'][b], population[i]['road'][a],
        else:
            for j in range(dist):
                population[i]['road'][a + j], population[i]['road'][b - j] = population[i]['road'][b - j], \
                                                                             population[i]['road'][a + j],

        population[i]['cost'] = self._calculate(population[i]['road'])

    # calculate price chromosome
    def _calculate(self, node):
        price = 0
        for i in range(1, len(node)):
            price += self.matrix[node[i]][node[i - 1]]
        return price

    def _get_new_child(self, parent1, parent2):
        first_cross_point = np.random.randint(0, len(parent1) - 2)
        second_cross_point = np.random.randint(first_cross_point + 1, len(parent1) - 1)

        parent1_middle_cross = parent1[first_cross_point:second_cross_point]
        parent2_middle_cross = parent2[first_cross_point:second_cross_point]

        temp_child1 = np.concatenate((parent1[:first_cross_point], parent2_middle_cross, parent1[second_cross_point:]),
                                     axis=0)
        temp_child2 = np.concatenate((parent2[:first_cross_point], parent1_middle_cross, parent2[second_cross_point:]),
                                     axis=0)

        relations = []
        for i in range(len(parent1_middle_cross)):
            relations.append([parent2_middle_cross[i], parent1_middle_cross[i]])

        def recursion(temp_child, first_cross_point, second_cross_point, parent1_middle_cross, parent2_middle_cross,
                      my_number):
            child = np.array([0 for i in range(len(parent1))])
            for i, j in enumerate(temp_child[:first_cross_point]):
                c = 0
                for x in relations:
                    if j == x[my_number]:
                        child[i] = x[(my_number + 1) % 2]
                        c = 1
                        break
                if c == 0:
                    child[i] = j
            j = 0
            for i in range(first_cross_point, second_cross_point):
                if my_number == 1:
                    child[i] = parent1_middle_cross[j]
                else:
                    child[i] = parent2_middle_cross[j]

                j += 1

            for i, j in enumerate(temp_child[second_cross_point:]):
                c = 0
                for x in relations:
                    if j == x[my_number]:
                        child[i + second_cross_point] = x[(my_number + 1) % 2]
                        c = 1
                        break
                if c == 0:
                    child[i + second_cross_point] = j
            child_unique = np.unique(child)
            if len(child) > len(child_unique):
                child = recursion(child, first_cross_point, second_cross_point, parent1_middle_cross,
                                  parent2_middle_cross,
                                  my_number)
            return child

        child1 = recursion(temp_child1, first_cross_point, second_cross_point, parent1_middle_cross,
                           parent2_middle_cross, 0)
        child2 = recursion(temp_child2, first_cross_point, second_cross_point, parent1_middle_cross,
                           parent2_middle_cross, 1)
        return child1, child2

    def start(self):
        self.population = []

        for i in range(self.count_population):
            self.population.append(
                {'road': np.random.choice(range(self.node_size), self.node_size, replace=False), 'cost': 0})
            self.population[i]['cost'] = self._calculate(self.population[i]['road'])

        j = 0
        replay = 0
        last_best = 0
        while self.iteration > replay:
            j += 1
            for i in range(self.count_population - 1):
                second = np.random.randint(self.count_population)
                while i == second:
                    second = np.random.randint(self.count_population)
                child = self._get_new_child(self.population[i]['road'], self.population[second]['road'])
                child1 = {'road': child[0], 'cost': 0}
                child2 = {'road': child[1], 'cost': 0}
                child1['cost'] = self._calculate(child1['road'])
                child2['cost'] = self._calculate(child2['road'])
                self.population.append(child1)
                self.population.append(child2)
            # mutation
            # start from 1 to not lose best one
            for i in range(1, len(self.population)):
                for k in range(1, len(self.population)):
                    if (self.population[i]['road'] == self.population[k]['road']).all() and not i == k:
                        self._mutation(i, self.population)
            nc = len(self.population)
            for i in range(nc):
                if my_rand.random() < self.mr:
                    self._mutation(i, self.population)

            #  selection
            self.population = sorted(self.population, key=lambda my_list: my_list["cost"])
            self.population = self.population[:self.count_population]
            if self.print_data:
                for i in self.population:
                    print(f'{i["road"]} ---> {i["cost"]}')
                print(f'--------------- step {j}  -----------------')
            if last_best == self.population[0]['cost']:
                replay += 1
            else:
                replay = 0
                last_best = self.population[0]['cost']
        print("************************************ DONE ************************************")

    def get_top_chromosome(self):
        self.population = sorted(self.population, key=lambda my_list: my_list["cost"])
        return self.population
